FROM rootproject/root-conda:6.18.04

COPY . analysis/skim

WORKDIR analysis/skim

RUN echo ">>> Compile skimming executables ..." &&  \
    COMPILER=$(root-config --cxx) &&  \
    FLAGS=$(root-config --cflags --libs) &&  \
    $COMPILER -g -std=c11 -O3 -Wall -Wextra -Wpedantic -o skim skim.cxx $FLAGS
